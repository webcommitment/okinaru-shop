<?php

$categories_args = array(
    'taxonomy' => 'product_cat' // the taxonomy we want to get terms of
);

$product_categories = get_terms($categories_args);

if ($product_categories) : ?>
    <section class="category">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="row category__wrapper">
                        <?php foreach ($product_categories as $product_category): ?>

                            <?php
                            $term_id = $product_category->term_id; // Term ID
                            $term_name = $product_category->name; // Term name
                            $term_desc = $product_category->description; // Term description
                            $term_link = get_term_link($product_category->slug, $product_category->taxonomy); // Term link\
                            $thumbnail_id = get_term_meta($product_category->term_id, 'thumbnail_id', true);
                            $term_thumb = wp_get_attachment_url($thumbnail_id); ?>

                            <div class=" col-card category__item animate-2">
                                <div class="category__card">
                                    <div class="category__thumbnail-wrapper">
                                        <a href="<?php echo $term_link; ?>">
                                            <?php if (!empty ($term_thumb)): ?>
                                                <img src="<?php echo $term_thumb; ?>"
                                                     alt="">
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                    <div class="category__content-wrapper">
                                        <div class="category__content-title">
                                            <h5>
                                                <?php echo $term_name; ?>
                                            </h5>
                                        </div>
                                        <div class="category__content-description">
                                            <p>
                                                <?php echo $term_desc; ?>
                                            </p>
                                        </div>
                                        <div class="category__content-cta">
                                            <a href="<?php echo $term_link; ?>" class="primary-btn">
                                                <?php echo __('DISCOVER MORE', 'webcommitment-theme'); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                        <?php
                        wp_reset_postdata(); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;
?>
