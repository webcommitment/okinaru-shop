<section class="with-img">
    <div class="d-flex row">
        <?php if ( !empty ( 'content_with_slider' ) ) : ?>
        <div class="col-12 col-lg-6">
            <div class="with-img__left">
                <div class="with-img__content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div id="" class="with-img__right">
                <?php if ( have_rows ( 'content_slider' ) ) : ?>
                <div class="">

                    <?php while (have_rows('content_slider')) : the_row();
                    $slide_image = get_sub_field('slide');?>
                    <div
                        class=" col-12 col-sm-12 col-md-11 col-lg-12 col-xl-12 px-0">
                        <div class="with-img__right-image mb-5">
                            <img src="<?php echo $slide_image['sizes']['wc-page-header'] ?>" alt="">
                        </div>

                    </div>
                    <?php

                    endwhile;
                    wp_reset_postdata();
                    ?>
                    <!-- container with icons -->
                </div>
                <?php endif; ?>

            </div>
        </div>
        <?php endif; ?>
    </div>
</section>