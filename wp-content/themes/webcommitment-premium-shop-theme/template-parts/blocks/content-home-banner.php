<?php
$posts = get_field('slider_content'); ?>
<?php if ($posts) : ?>
    <?php if (is_front_page()) { ?>
        <section class="home-banner">
            <div id="home-banner-slider" class="home-banner__carousel carousel carousel-fade  slide"
                 data-ride="carousel">
                <div class="home-banner__carousel-indicators">
                    <ol class="carousel-indicators">
                        <?php
                        foreach ($posts
                                 as $index => $post) :
                            ?>
                            <li data-target="#home-banner-slider" data-slide-to=<?php echo $index++ ?>
                            class="<?php echo($index == 0 ? 'active' : ''); ?>"></li>
                            <?php
                            ?>
                        <?php endforeach; ?>
                    </ol>
                </div>
                <div class="carousel-inner">
                    <?php foreach ($posts

                                   as $index => $post) :
                        ?>
                        <?php $bg = get_field('slider_bg', $post->ID); ?>
                        <div class="carousel-item <?php echo($index == 0 ? 'active' : ''); ?>"
                             style="background-image: url('<?php echo $bg['url'] ?>');">

                            <?php
                            $title = get_field('slider_title', $post->ID);
                            $content = get_field('slider_content', $post->ID);
                            $icon = get_field('slider_icon', $post->ID);
                            $btn_link = get_field('slider_button_link', $post->ID);;
                            $btn_text = get_field('slider_button_tekst', $post->ID);;
                            $post_id = $post->ID; ?>
                            <div class="home-banner__carousel-content">
                                <div class="home-banner__carousel-inner">
                                    <div class="home-banner__carousel-inner--icon">
                                        <img src=" <?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
                                    </div>
                                    <h3 class="animate">  <?php echo $title; ?> </h3>
                                    <div class="animate">
                                        <p>
                                            <?php echo $content; ?>
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    <?php } ?>
<?php endif; ?>