<section class="with-img">
    <div class="d-flex row">
        <?php if ( !empty ( 'content_with_slider' ) ) : ?>
        <div class="col-12 col-lg-6">
            <div class="with-img__left">
                <div class="with-img__content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div id="page-slider" class="with-img__right carousel slide carousel-fade" data-ride="carousel">
                <?php if ( have_rows ( 'content_slider' ) ) : ?>
                <div class="carousel-inner">
                    <?php
                $l = 0; ?>
                    <?php while (have_rows('content_slider')) : the_row();
                    $slide_image = get_sub_field('slide');?>
                    <div
                        class=" col-12 col-sm-12 col-md-11 col-lg-12 col-xl-12 carousel-item <?php echo($l == 0 ? 'active' : ''); ?>  px-0">
                        <div class="with-img__right-image">
                            <img src="<?php echo $slide_image['sizes']['wc-page-header'] ?>" alt="">
                        </div>
                        <div class="home-banner__carousel-nav">
                            <a class="carousel-control-prev disable-anim" href="#page-slider" role="button"
                                data-slide="prev">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next disable-anim" href="#page-slider" role="button"
                                data-slide="next">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <?php 
                    $l++;
                    endwhile;
                    wp_reset_postdata();
                    ?>
                    <!-- container with icons -->
                </div>
                <?php endif; ?>

            </div>
        </div>
        <?php endif; ?>
    </div>
</section>