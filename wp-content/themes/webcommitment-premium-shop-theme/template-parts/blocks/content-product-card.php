<?php
global $product;
?>

<a href="<?php echo get_permalink($product->ID); ?>">
    <div class="product-item__product-card">
        <div class="product-item__image">
            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($product->ID), 'single-post-thumbnail'); ?>
            <img src="<?php echo $image[0]; ?>" data-id="<?php echo $product->post->ID; ?>">
        </div>
        <div class="product-item__details">
            <h3 class="product-item__title">
                <?php echo get_the_title($product->post->ID); ?>
            </h3>
        </div>


        <div class="product-item__button">
           <a class='primary-btn' href=" <?php echo get_permalink($product->ID); ?>">SEE MORE</a>
</div>
</div>
</a>