<?php $map = get_field( 'map');?>


<section class="with-img">
    <div class="d-flex row">
        <?php if(!empty ($map)): ?>
        <div class="col-12 col-lg-6 ">
            <div class="with-img__left">
                <div class="list">
                    <?php echo the_content(); ?>
                </div>
                <div class="contact-form">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <?php
                            // if ( ICL_LANGUAGE_CODE == 'en' )
                            echo do_shortcode( '[contact-form-7 id="241" title="Contact formulier"]' ); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=" col-12 col-lg-6">
            <div class="with-img__right">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0">
                    <div class="with-img__right-map">
                        <?php echo $map;?>
                    </div>
                </div>

                <!-- container with icons -->
                <div class="col-10 col-sm-12 col-md-11 col-lg-12 col-xl-12 m-auto px-0">
                    <div class="with-img__right-icons ">
                        <?php if (have_rows('main_ctas', 'option')): ?>
                        <div class="container-fluid">
                            <h3>
                                <?php echo __('CONTACT US NOW', 'webcommitment-theme'); ?>
                            </h3>
                            <div class="newsletter__icons">
                                <?php while (have_rows('main_ctas', 'option')): the_row();
                                                    $icon = get_sub_field('icon', 'option');
                                                    $link = get_sub_field('link', 'option');
                                                    ?>
                                <div class="newsletter__icons-item with-img__right-icons-item ">
                                    <a class="" href="<?php echo $link['url']; ?>"
                                        aria-label=" <?php echo $link['title']; ?>">
                                        <div class="contacts-block-small__icon">
                                            <img src="<?php echo $icon['url']; ?>"
                                                alt="<?php echo $link['title']; ?>" />
                                        </div>
                                    </a>
                                </div>
                                <?php endwhile; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
</section>