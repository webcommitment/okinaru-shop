<?php
$posts = get_field('slider_content'); ?>
<?php if (is_array($posts) || is_object($posts)) : ?>
    <section id="section-slider">

        <?php if ($posts) : ?>
            <div id="frontCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php if (is_array($posts) || is_object($posts)) : ?>
                        <?php foreach ($posts
                                       as $index => $post) :
                            ?>
                            <?php $bg = get_field('slider_bg', $post->ID); ?>
                            <div class="carousel-item <?php echo ($index == 0) ? 'active' : '' ?>"
                                 style="background-image: url('<?php echo $bg['url'] ?>');">
                                <?php
                                $title = get_field('slider_title', $post->ID);
                                $content = get_field('slider_content', $post->ID);
                                $btn_link = get_field('slider_button_link', $post->ID);;
                                $btn_text = get_field('slider_button_tekst', $post->ID);;
                                $post_id = $post->ID; ?>
                                <div class="row row-slider justify-content-center">
                                    <div class="col-10 col-slider">
                                        <div class="slider-wrap">
                                            <div class="slider-inner">
                                                <div class="slider-title">
                                                    <?php echo $title; ?>
                                                </div>
                                                <div class="slider-content">
                                                    <p>
                                                        <?php echo $content; ?> </p>
                                                </div>
                                                <div class="slider-cta">
                                                    <a href="<?php echo $btn_link['url']; ?>"
                                                       class="secondary-btn ">
                                                        <?php echo $btn_text; ?>                                        </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <a class="carousel-control-prev disable-anim" href="#frontCarousel" role="button"
                   data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next disable-anim" href="#frontCarousel" role="button"
                   data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <ol class="carousel-indicators">
                    <?php foreach ($posts
                                   as $index => $post) :
                        ?>
                        <li data-target="#frontCarousel"
                            data-slide-to=<?php echo $index++ ?>>
                        </li>
                    <?php endforeach; ?>
                </ol>
            </div>
        <?php endif; ?>
    </section>
<?php endif; ?>
