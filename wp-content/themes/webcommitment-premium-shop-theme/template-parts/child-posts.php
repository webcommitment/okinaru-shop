<?php

$args = array(
	'posts_per_page' => - 1,
	'post_type'      => 'post',
	'orderby'        => 'date',
	'order' => 'DESC'
);

if ( is_category() ){
	$blog = get_queried_object();
	$blog_id = $blog->cat_id;

	$args = array(
		'cat' => get_query_var('cat'),
		'orderby' => 'title',
		'order' => 'ASC',
		'posts_per_page'=>-1,
		'caller_get_posts'=>1
	);
}

$children = get_posts($args);

$child_count = count($children);

$col = 'col-sm-6 col-lg-4 col-12';

if ( $children ){
	$pages = $children;
	if ( $child_count === 2 || $child_count === 4) {
		$col = 'col-md-6 col-sm-12 col-lg-4';
	}
}

else{
	echo 'Er zijn nog geen nieuwsberichten.';
}

?>
<div class="page-blog__wrapper">
    <div class="row">
        <?php
	foreach ( $pages as $page ) :
		$post_id = $page->ID;
		$post_title = $page->post_title;
		$post_thumb = get_the_post_thumbnail_url( $post_id );
		if ( !$post_thumb ){
			$post_thumb = '/wp-content/themes/webcommitment-theme/img/placeholder_thumb.jpg';
		}
		$post_excerpt = $page->post_excerpt;
		$post_link = get_the_permalink( $post_id ); ?>

        <div class="<?php echo $col; ?> col-card">
            <div class="content-card">
                <div class="card-title">
                    <h2>
                        <?php echo $post_title; ?>
                    </h2>
                </div>
                <div class="card-thumb">
                    <a href="<?php echo $post_link; ?>">
                        <img src="<?php echo $post_thumb; ?>" alt="<?php echo $post_title; ?>">
                    </a>
                    <?php
                        $postcat = get_the_category( $post_id ); ?>
                    <?php
                        if ( ! empty( $postcat ) ) {
                            echo '<a href="/category/'.$postcat[0]->slug.'"><span class="cat">';
	                        echo esc_html( $postcat[0]->name );
	                        echo '</span></a>';
                        }
                        ?>
                    <span class="date">
                        <?php echo get_the_date('d / m  / Y', $post_id); ?>
                    </span>
                </div>
                <div class="card-excerpt">
                    <p>
                        <?php echo $post_excerpt; ?>
                    </p>
                </div>
                <div class="card-cta text-center">
                    <a href="<?php echo $post_link; ?>" class="secondary-btn ">
                        <?php echo __('Lees meer', 'webcommitment-theme'); ?>
                    </a>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>