<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" class="post-content">

    <header class="entry-header"
            style="background:linear-gradient(74deg, rgba(0,0,0,0.8) 30%, rgba(0,0,0,0.2) 60%), url('<?php echo get_the_post_thumbnail_url(); ?>');">
        <div class="container-fluid row align-items-center">
            <!-- page icon -->
            <?php
            $icon_page = get_field( 'icon'); ?>

            <?php if(!empty ($icon_page)): ?>
                <div class="entry-header__icon">
                    <img src="<?php echo $icon_page['url']; ?>" alt="" />
                </div>
            <?php endif; ?>
            <!-- end page icon -->
            <div class="entry-header__title">
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                <div class="breadcrumbs">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </header><!-- .entry-header -->


    <section class="entry-content">
        <div class="container-fluid">
            <?php get_template_part('template-parts/blocks/content', 'page-map');
            ?>
            <?php wp_link_pages(array(
                'before' => '<div class="page-links">' . esc_html__('Pages:', 'webcommitment-theme'),
                'after' => '</div>',
            ));
            ?>
        </div>
    </section>
</article>