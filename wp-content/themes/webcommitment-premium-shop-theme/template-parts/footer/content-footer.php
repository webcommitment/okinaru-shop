<?php


if ( have_rows( 'global_settings', 'option' ) ) :
	while ( have_rows( 'socials', 'option' ) ) : the_row();
		$facebook_link  = get_sub_field( 'facebook_link' );
		$twitter_link   = get_sub_field( 'twitter_link' );
		$instagram_link = get_sub_field( 'instagram_link' );
		$linkedin_link  = get_sub_field( 'linkedin_link' );
	endwhile;
endif;
?>

<!-- footer bottom -->
<div class="row footer">
    <!-- address -->
    <div class="col-12 col-md-5 col-lg-6">
        <div class="footer__item">
            <p>Pro Template B.V. &copy; 2021</p>
        </div>
    </div>
    <div class="col-12 col-md-7 col-lg-6">
        <div class="footer__item">
            <div class="row">
                <div class="col-12 text-align-right">
                    <ul class="footer__item-links ">
                        <li>
                            <a href="/algemene-voorwaarden">
                                <?php echo __('Algemene voorwaarden', 'webcommitment-theme'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="/privacy-policy">
                                <?php echo __('Privacy beleid', 'webcommitment-theme'); ?>
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- social links -->

            </div>
        </div>
    </div>
</div>