<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package webcommitment_Starter
 */

get_header(); ?>
	<article id="search">
        <section class="main-content">
            <h1>
                Zoekresultaten voor: <?php get_search_query(); ?>
            </h1>
	        <?php if ( have_posts() ) : ?>
		        <?php
		        while ( have_posts() ) : the_post();
			        get_template_part( 'template-parts/content', 'search' );
		        endwhile;
	        else :
		        get_template_part( 'template-parts/content', 'none' );
	        endif; ?>
        </section>
    </article>
<?php
get_footer();
