<?php

function create_slides()
{
    $e_args = array(
        'labels' => array(
            'name' => __('Slides', 'webcommitment-theme'),
            'singular' => __('Slide', 'webcommitment-theme'),
            'new_item' => __('New Slide', 'webcommitment-theme'),
            'add_new_item' => __('Add new Slide', 'webcommitment-theme'),
            'add_new' => __('Add new', 'webcommitment-theme'),
            'edit_item' => __('Edit slide', 'webcommitment-theme'),
            'view_item' => __('View slide', 'webcommitment-theme'),
            'all_items' => __('All slides', 'webcommitment-theme'),
            'search_items' => __('Search slides', 'webcommitment-theme'),
            'not_found' => __('No slides found.', 'webcommitment-theme'),
            'not_found_in_trash' => __('No slides found in trash.', 'webcommitment-theme')
        ),
        'public' => true,
        'has_archive' => false,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'supports' => array('title', 'thumbnail'),
        'menu_position' => 5,
        'hierarchical' => false,
        'menu_icon' => 'dashicons-groups'
    );
//Registers CPTs
    register_post_type('slides', $e_args);
}

add_action('init', 'create_slides');

add_post_type_support('page', 'excerpt');


add_theme_support('wc-product-gallery-zoom');
add_theme_support('wc-product-gallery-lightbox');
add_theme_support('wc-product-gallery-slider');

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);


function categories_postcount_filter($variable)
{
    $variable = str_replace('(', '<span class="post_count"> ', $variable);
    $variable = str_replace(')', ' </span>', $variable);
    return $variable;
}

add_filter('wp_list_categories', 'categories_postcount_filter');

add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);
function woo_remove_product_tabs($tabs)
{
    unset($tabs['reviews']);          // Remove the reviews tab
    return $tabs;
}

//remove additional information
add_filter( 'woocommerce_product_tabs', 'bbloomer_remove_product_tabs', 99 );

function bbloomer_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );
    return $tabs;
}

//add_action('woocommerce_after_single_product_summary', '_show_reviews', 15);
//function _show_reviews()
//{
//    comments_template();
//}


add_filter('woocommerce_cart_item_subtotal', 'ts_show_product_discount_order_summary', 10, 3);

function ts_show_product_discount_order_summary($total, $cart_item, $cart_item_key)
{

    //Get product object
    $_product = $cart_item['data'];

    //Check if sale price is not empty
    if ('' !== $_product->get_sale_price()) {

        //Get regular price of all quantities
        $regular_price = $_product->get_regular_price() * $cart_item['quantity'];

        //Prepend the crossed out regular price to actual price
        $total = $total . '<span style="text-decoration: line-through; opacity: 0.5; padding-right: 5px;">' . wc_price($regular_price) . '</span>';
    }

    // Return the html
    return $total;
}


remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_action('woocommerce_after_product_reviews', 'woocommerce_output_related_products', 5);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
add_action('woocommerce_after_related_products', 'woocommerce_upsell_display', 10);

