<?php
/**
 * Additional features to allow styling of the templates
 *
 * @package webcommitment_Starter
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function webcommitment_starter_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'webcommitment_starter_body_classes' );

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Theme settings');
}

/**
 *  Register Image sizes
 */

add_image_size( 'wc-page-header', 960, 825, true );