<?php
/**
 * Template Name: Page with children
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */


get_header(); ?>
    <article id="main-page">
        <section class="main-content">

            <?php $icon_page = get_field('icon');
            ?>

            <article id="post-<?php the_ID(); ?>" class="post-content">
                <header class="entry-header" style="background:linear-gradient(74deg, rgba(0,0,0,0.8) 30%, rgba(0,0,0,0.2) 60%), url('<?php echo get_the_post_thumbnail_url(); ?>');">
                    <div class="container-fluid row align-items-center">
                        <!-- page icon -->
                        <?php if (!empty ($icon_page)): ?>
                            <div class="entry-header__icon">
                                <img src="<?php echo $icon_page['url']; ?>" alt=""/>
                            </div>
                        <?php endif; ?>
                        <!-- end page icon -->
                        <div class="entry-header__title">
                            <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                            <div class="breadcrumbs">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    </div>
                </header><!-- .entry-header -->
            </article>
            <?php get_template_part('template-parts/child-posts-projects'); ?>

        </section>
    </article>
<?php get_template_part('template-parts/blocks/content', 'slider-page');?>
<?php get_template_part('template-parts/blocks/content', 'home-usps-icons'); ?>

<?php
get_footer();