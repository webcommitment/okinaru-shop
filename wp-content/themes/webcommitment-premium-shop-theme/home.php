<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

get_header();

$blog = get_queried_object();
$blog_id = $blog->ID;

?>
    <article id="page-blog" class="blog-content">
        <header class="entry-header page__title"    style="background:linear-gradient(74deg, rgba(0,0,0,0.8) 30%, rgba(0,0,0,0.2) 60%), url('<?php echo get_the_post_thumbnail_url(); ?>');">
            <div class="container-fluid row align-items-center">
                <!-- page icon -->
                <?php
                $icon_page = get_field('icon', $blog_id); ?>

                <?php if (!empty ($icon_page)): ?>
                    <div class="entry-header__icon">
                        <img src="<?php echo $icon_page['url']; ?>" alt=""/>
                    </div>
                <?php endif; ?>
                <!-- end page icon -->
                <div class="entry-header__title">
                    <h1 class="entry-title "> <?php echo get_the_title($blog_id); ?></h1>
                    <div class="breadcrumbs">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </header><!-- .entry-header -->
        <?php get_template_part('template-parts/content', 'newsarchive'); ?>
    </article>
<?php get_template_part('template-parts/blocks/content', 'slider-page');?>
<?php get_template_part('template-parts/blocks/content', 'home-usps-icons'); ?>
<?php
get_footer();