<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webcommitment_Starter
 */

// if ( ! is_active_sidebar( 'sidebar-1' ) ) {
//     return;
// }
// dynamic_sidebar( 'sidebar-1' );
// ?>
<!-- 
<aside id="main-sidebar" class="widget-area" role="complementary"> -->

<aside class="breadcrumb-wrapper">
        <div class="row justify-content-start">
            <div class="col-12 col-md-11 col-xl-10">
                <div class="crumb-inner">
					<?php yoast_breadcrumb(); ?>
                </div>
            </div>
        </div>
</aside>