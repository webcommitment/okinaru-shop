<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
global $post;
$terms = get_the_terms($post->ID, 'product_cat');

foreach ($terms as $term) {
    $product_cat_id = $term->term_id;
    $thumbnail_id = get_term_meta($product_cat_id, 'thumbnail_id', true);
    $image_url = wp_get_attachment_url($thumbnail_id); // This variable is returing the product category thumbnail image URL.
}
$tax_id = $terms->term_id;

get_header('shop'); ?>

    <article class="post-content">
        <header class="entry-header"
                style="background:linear-gradient(74deg, rgba(0,0,0,0.8) 30%, rgba(0,0,0,0.2) 60%), url('<?php echo get_the_post_thumbnail_url($tax_id); ?>');">
            <div class="container-fluid row align-items-center">
                <!-- page icon -->
                <?php if (!empty ($icon_page)): ?>
                    <div class="entry-header__icon">
                        <img src="<?php echo $icon_page['url']; ?>" alt=""/>
                    </div>
                <?php endif; ?>
                <!-- end page icon -->
                <div class="entry-header__title">
                    <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                    <div class="breadcrumbs">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </header><!-- .entry-header -->
        <section id="single-product-page" class="page-banner page-banner-product-single"
                 style="background-image: url('<?php echo $image_url; ?>');">
        </section>
        <section class="single-product-wrapper">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12">

                        <?php while (have_posts()) : ?>
                            <?php the_post(); ?>
                            <?php wc_get_template_part('content', 'single-product'); ?>

                        <?php endwhile; // end of the loop. ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section review -->

        <section class="single-product-wrapper__related product-item">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <?php do_action('woocommerce_after_product_reviews'); ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="single-product-wrapper__upsell product-item">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <?php do_action('woocommerce_after_related_products'); ?>
                    </div>
                </div>
            </div>
        </section>

        <?php get_template_part('template-parts/blocks/content', 'slider-page'); ?>
        <?php get_template_part('template-parts/blocks/content', 'home-usps-icons'); ?>
    </article>
<?php
get_footer('shop');