<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

$shop_id = get_option( 'woocommerce_shop_page_id' );
$term    = get_queried_object();
$tax_id  = $term->term_id;
$excerpt = $term->description;

$thumbnail_id = get_term_meta( $tax_id, 'thumbnail_id', true );
$image_url    = wp_get_attachment_url( $thumbnail_id ); // This variable is returing the product category thumbnail image URL.

if ( is_shop() || ! $image_url ) {
	$image_url = get_the_post_thumbnail_url( $shop_id );
}

if ( is_shop() || ! $excerpt ) {
	$excerpt = get_the_excerpt( $shop_id );
}
?>

<article>
    <section id="woo-page" class="page-banner post-content">
        <header class="entry-header"  style="background:linear-gradient(74deg, rgba(0,0,0,0.8) 30%, rgba(0,0,0,0.2) 60%), url('<?php echo get_the_post_thumbnail_url($shop_id); ?>');">
            <div class="container-fluid row align-items-center">
                <!-- page icon -->
                <?php
                $icon_page = get_field( 'icon', $shop_id );
                ?>
                <?php if(!empty ($icon_page)): ?>
                <div class="entry-header__icon">
                    <img src="<?php echo $icon_page['url']; ?>" alt="" />
                </div>
                <?php endif; ?>
                <!-- end page icon -->
                <div class="entry-header__title">
                    <h1 class="entry-title">
                        <?php woocommerce_page_title(); ?>
                    </h1>
                </div>
                <!-- To do BREADCRUMBS -->
            </div>
        </header>

    </section>
    <section id="page-woo" class="shop">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="row">
                        <div class="filter-toggle col-12 col-sm-10 col-md-4 col-lg-3" id="shop-filter-btn">
                            <div class="shop__filters-wrapper">
                                <?php echo __('','webcommitment-theme')?>
                                <?php get_template_part( 'template-parts/content', 'sidebar' ); ?>
                            </div>
                        </div>

                        <div class="col-12 col-sm-10 col-md-8 col-lg-9">
                            <div class="shop__products-wrapper">
                                <?php
								if ( woocommerce_product_loop() ) {

									/**
									 * Hook: woocommerce_before_shop_loop.
									 *
									 * @hooked woocommerce_output_all_notices - 10
									 * @hooked woocommerce_result_count - 20
									 * @hooked woocommerce_catalog_ordering - 30
									 */
									do_action( 'woocommerce_before_shop_loop' );

									woocommerce_product_loop_start();

									if ( wc_get_loop_prop( 'total' ) ) {
										echo '<div class="row">';
										while ( have_posts() ) {
											echo '<div class="col-12 col-md-6 col-lg-4">';
											the_post();

											/**
											 * Hook: woocommerce_shop_loop.
											 */
											do_action( 'woocommerce_shop_loop' );
												wc_get_template_part( 'template-parts/blocks/content', 'product-card' );
											echo '</div>';
										}
										echo '</div>';
									}

									woocommerce_product_loop_end(); ?>

                                <?php
									/**
									 * Hook: woocommerce_after_shop_loop.
									 *
									 * @hooked woocommerce_pagination - 10
									 */
									do_action( 'woocommerce_after_shop_loop' );
								} else {
									/**
									 * Hook: woocommerce_no_products_found.
									 *
									 * @hooked wc_no_products_found - 10
									 */
									do_action( 'woocommerce_no_products_found' );
								} ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
<?php get_template_part('template-parts/blocks/content', 'slider-page');?>
<?php get_template_part('template-parts/blocks/content', 'home-usps-icons'); ?>

<?php get_footer( 'shop' ); ?>