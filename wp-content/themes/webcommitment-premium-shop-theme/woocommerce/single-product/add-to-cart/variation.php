<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

global $product;
$mail = get_field('option_mail', 'option');
if (!$product->is_purchasable()) {
    return;
}

echo wc_get_stock_html($product); // WPCS: XSS ok.

if ($product->is_in_stock()) : ?>

    <?php do_action('woocommerce_before_add_to_cart_form'); ?>
    <form class="cart"
          action="<?php echo esc_url(apply_filters('woocommerce_add_to_cart_form_action', $product->get_permalink())); ?>"
          method="post" enctype='multipart/form-data'>
        <?php do_action('woocommerce_before_add_to_cart_button'); ?>
        <div class="product-button-wrapper">
            <a class='secondary-btn '
               value="<?php echo esc_attr($product->get_id()); ?>"
               href="mailto:<?php echo $mail; ?>" target="_blank"><?php echo __('ENQUIR', 'webcommitment-theme'); ?></a>
        </div>
        <?php do_action('woocommerce_after_add_to_cart_button'); ?>
    </form>
    <?php do_action('woocommerce_after_add_to_cart_form'); ?>
<?php endif; ?>
