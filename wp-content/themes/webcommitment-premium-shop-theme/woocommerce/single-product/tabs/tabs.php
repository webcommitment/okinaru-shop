<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );
$i            = 0;
if ( ! empty( $product_tabs ) ) : ?>

		<div class="panel-group product-collapse">
			<div class="panel panel-default product-collapse__item">
				<div class="panel-heading product-collapse__heading" id="tab-title-<?php echo esc_attr( $key ); ?>">
								<a data-toggle="collapse" href="#tab-<?php echo esc_attr( $key ); ?>"
									class="product-collapse__toggle <?php echo ( $i == 0 ) ? 'open' : ''; ?>"
								>
									Product USP's
									<span class="collapse-arrow"></span>
								</a>
							</div>
							<div id="tab-<?php echo esc_attr( $key ); ?>"
								class="panel-collapse collapse <?php echo ( $i == 0 ) ? 'show' : ''; ?>
									product-collapse__body
								">
								<div class="panel-body">
									<?php
									if ( have_rows( 'product_usps' ) ) :
										?>
									<ul class="slides">
										<?php
										while ( have_rows( 'product_usps' ) ) :
											the_row();
											?>
											<li>
												<?php the_sub_field( 'item_usp' ); ?>
											</li>
											<?php endwhile; ?>
									</ul>
									<?php endif; ?>
								</div>
							</div>
			</div>
			

		<?php foreach ( $product_tabs as $key => $product_tab ) : ?>
			<div class="panel panel-default product-collapse__item">
				<div class="panel-heading product-collapse__heading" id="tab-title-<?php echo esc_attr( $key ); ?>">
					<a data-toggle="collapse" href="#tab-<?php echo esc_attr( $key ); ?>"
						class="product-collapse__toggle <?php echo ( $i == 0 ) ? 'open' : ''; ?>"
					>
						<?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?>
						<span class="collapse-arrow"></span>
					</a>
				</div>
				<div id="tab-<?php echo esc_attr( $key ); ?>"
					 class="panel-collapse collapse <?php echo ( $i == 0 ) ? 'show' : ''; ?>
						product-collapse__body
					">
					<div class="panel-body">
						<?php
						if ( isset( $product_tab['callback'] ) ) {
							call_user_func( $product_tab['callback'], $key, $product_tab );
						}
						?>
					</div>
				</div>
			</div>
			<?php
			$i++;
		endforeach;
		?>
		</div>

    <?php do_action('woocommerce_product_after_tabs'); ?>
<?php endif; ?>
