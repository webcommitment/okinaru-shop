<?php

$option_companyName = get_field('option_company_name', 'option');
$option_companyDescription = get_field('option_company_description', 'option');
$option_address = get_field('option_address', 'option');
$option_postcode = get_field('option_postcode', 'option');
$option_city = get_field('option_city', 'option');
$option_mail = get_field('option_mail', 'option');
$option_tel = get_field('option_tel', 'option');
$extra_logo = get_field('extra_logo', 'option');

$shop_id = get_option('woocommerce_shop_page_id');

$thumb = get_the_post_thumbnail_url();

if (is_home()) {
    $blog = get_queried_object();
    $blog_id = $blog->ID;
    $thumb = get_the_post_thumbnail_url($blog_id);
}
if (is_shop()) {
    $thumb = get_the_post_thumbnail_url($shop_id);
}

?>

</div><!-- #content -->
</main>
<footer style="background-image: url('<?php echo $thumb; ?>')">
    <div id="footer-wrapper">
        <div class="footer-top">
            <div class="container-fluid ">
                <div class="row justify-content-between">
                    <div class="col-12">
                        <?php get_template_part('template-parts/footer/content', 'prefooter'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom ">
            <div class="container-fluid ">
                <div class="row justify-content-between">
                    <div class="col-12">
                        <?php get_template_part('template-parts/footer/content', 'footer'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>

</html>